coreos-image-builder
====================

This repository has been decomissioned as it is no longer active,
the tooling and support has been long since removed by the ironic
community, and CoreOS Container Linux itself has announced it's
End-Of-Life.

Users looking to build ironic-python-agent ramdisks should consult
the Ironic-Python-Agent-Builder:

* https://docs.openstack.org/ironic-python-agent-builder/latest/

The contents of this repository can be retrieved using the command
'git reset --hard HEAD~1' command.
